/*
 * Copyright (c) 2012, Hinnerk van Bruinehsen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdbool.h>
#include <stdio.h>
#include <syslog.h>

#define SELINUX_STATE "/sys/fs/selinux/enforce"

static FILE *selinuxFd = 0;

bool
plugin_init()
{
    selinuxFd = fopen(SELINUX_STATE, "r");
    if (selinuxFd != 0) {
        syslog(LOG_INFO, "SELINUX state can not be initialized.");
    }

    return selinuxFd != 0;
}

void
plugin_exit()
{
    fclose(selinuxFd);
}

const char *
plugin_status()
{
    static char buf[4];
    static unsigned int cycle = 0;

    int state;

    if (cycle++ % 5 != 0 || selinuxFd == 0) {
        return buf;
    }

    fflush(selinuxFd);
    rewind(selinuxFd);

    if (fscanf(selinuxFd, "%10i", &state) != 1) {
        syslog(LOG_ERR, "error reading SELINUX status, reopening\n");

        selinuxFd = freopen(SELINUX_STATE, "r", selinuxFd);
        return "";
    }

    if (state == 1) {
        snprintf(buf, 4, "[E]");
    } else {
        if (state == 0)
        snprintf(buf, 4, "[P]");
    }

    return buf;
}

char
plugin_format()
{
    return 's';
}
