/*
 * Copyright (c) 2016, Hinnerk van Bruinehsen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIrecvT, INDIrecvT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFSIZE 255

void print_speed(char *speedstr, long long new, long long old);
int parse_netdev(long long *received, long long *sent);

static long long recv, sent;
static FILE *proc_fd;

bool
plugin_init()
{
	proc_fd = fopen("/proc/net/dev", "r");
	recv = 0;
	sent = 0;

    return true;
}

void
plugin_exit()
{
	fclose(proc_fd);
}

const char *
plugin_status()
{
    static char buf[200];
	char downspeedstr[15], upspeedstr[15];
	long long newrecv, newsent;
	newrecv = newsent = 0;

	int retval = parse_netdev(&newrecv, &newsent);
	if (retval) {
		fprintf(stderr, "Error parsing /proc/net/dev.\n");
		return "";
	}

	print_speed(downspeedstr, newrecv, recv);
	print_speed(upspeedstr, newsent, sent);

	sprintf(buf, "%s | %s", upspeedstr, downspeedstr);

	recv = newrecv;
	sent = newsent;

    return buf;
}

void
print_speed(char *tgt_buf, long long new, long long old) {
	char prefix = 'K';
	double speed = (new - old)/1024.0;
	if (speed > 1024.0) {
		speed /= 1024.0;
		prefix = 'M';
	}
	sprintf(tgt_buf, "%.2f %cB/s", speed, prefix);
}

int parse_netdev(long long *received, long long *sent) {
	char buf[BUFSIZE];
	char *datastart;
	int rval = 1;
	long long receivedacc, sentacc;

	rewind(proc_fd);
	fflush(proc_fd);

	/* Ignore first two lines, reopen stream on error */
	fgets(buf,BUFSIZE,proc_fd);
	if (fgets(buf,BUFSIZE,proc_fd) == NULL) {
		proc_fd = freopen("/proc/net/dev", "r", proc_fd);
		return 0;
	}

	while (fgets(buf, BUFSIZE, proc_fd)) {
		if ((datastart = strstr(buf, "lo:")) == NULL) {
			datastart = strstr(buf, ":");

			sscanf(datastart + 1, "%llu  %*d     %*d  %*d  %*d  %*d   %*d        %*d       %llu",\
					&receivedacc, &sentacc);
			*received += receivedacc;
			*sent += sentacc;
			rval = 0;
		}
	}
	return rval;
}

char
plugin_format()
{
    return 'n';
}
