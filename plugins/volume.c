/*
 * Copyright (c) 2012-2013, Patrick Steinhardt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <alloca.h>
#include <stdbool.h>
#include <alsa/asoundlib.h>

#include "../config.h"

static snd_mixer_t *mixer = NULL;

bool
plugin_init()
{
    if (snd_mixer_open(&mixer, 0) != 0) {
        return false;
    }

    snd_mixer_attach(mixer, "default");
    snd_mixer_selem_register(mixer, 0, 0);
    snd_mixer_load(mixer);

    return true;
}

void
plugin_exit()
{
    snd_mixer_close(mixer);
}

const char *
plugin_status()
{
    static char buf[VOL_LEN];
    static char logo[5];
    snd_mixer_selem_id_t *elementId;
    snd_mixer_elem_t *element;
    long p, volume, min, max;
    int unmuted;

    snd_mixer_handle_events(mixer);

    snd_mixer_selem_id_alloca(&elementId);
    snd_mixer_selem_id_set_index(elementId, 0);
    snd_mixer_selem_id_set_name(elementId, "Master");
    element = snd_mixer_find_selem(mixer, elementId);
    if (element == NULL) {
        return NULL;
    }

    snd_mixer_selem_get_playback_switch(element, 0, &unmuted);
    if (!unmuted) {
        return NULL;
    }

    snd_mixer_selem_get_playback_volume(element, 0, &volume);
    snd_mixer_selem_get_playback_volume_range(element, &min, &max);

    p = ((volume - min) * 100) / (max - min);
    snprintf(logo, 5, "%s  ", VOL_FULL);
    if (p <= 66)
        snprintf(logo, 5, "%s  ", VOL_HALF);
    if (p <= 33)
        snprintf(logo, 5, "%s  ", VOL_EMPTY);
    snprintf(buf, VOL_LEN, VOL_FORMAT, logo, p);

    return buf;
}

char
plugin_format()
{
    return 'v';
}
