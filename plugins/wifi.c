/*
 * Copyright (c) 2013, Patrick Steinhardt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <ctype.h>
#include <stdbool.h>
#include <syslog.h>

#include <net/if.h>

#include <netlink/msg.h>
#include <netlink/genl/ctrl.h>
#include <netlink/genl/genl.h>

#include <linux/nl80211.h>

#include "../config.h"

#define UNUSED(x) (void)(x)
#define SSID_LEN 32

static struct nl_sock *sock;
static struct nl_cb *cb;
static int family;
static char ssid[SSID_LEN + 1];

static bool finished;
static bool associated;

static int scan_cb(struct nl_msg *msg, void *arg);
static int ack_cb(struct nl_msg *msg, void *arg);

bool
plugin_init()
{
    sock = nl_socket_alloc();
    if (sock == NULL) {
        syslog(LOG_ERR, "Error allocating socket");
        return false;
    }

    if (genl_connect(sock) != 0) {
        syslog(LOG_ERR, "Error connecting socket");
        return false;
    }

    cb = nl_cb_alloc(NL_CB_CUSTOM);
    if (cb == NULL) {
        syslog(LOG_ERR, "Error allocating cb");
        return NULL;
    }
    nl_cb_set(cb, NL_CB_VALID, NL_CB_CUSTOM, scan_cb, NULL);
    nl_cb_set(cb, NL_CB_FINISH, NL_CB_CUSTOM, ack_cb, NULL);
    nl_cb_set(cb, NL_CB_ACK, NL_CB_CUSTOM, ack_cb, NULL);

    family = genl_ctrl_resolve(sock, "nl80211");

    return true;
}

void
plugin_exit()
{
    free(cb);
    nl_socket_free(sock);
}

const char *
plugin_status()
{
    struct nl_msg *msg;

    msg = nlmsg_alloc();
    if (msg == NULL) {
        return false;
    }

    genlmsg_put(msg, 0, 0, family, 0, NLM_F_DUMP, NL80211_CMD_GET_SCAN, 0);
    NLA_PUT_U32(msg, NL80211_ATTR_IFINDEX, if_nametoindex(WIRELESS_IFACE));

    if (nl_send_auto_complete(sock, msg) < 0) {
        nlmsg_free(msg);
        return NULL;
    }

    associated = false;
    finished = false;

    if (nl_recvmsgs(sock, cb) != 0) {
        nlmsg_free(msg);
        return NULL;
    }

    if (!finished)
        nl_wait_for_ack(sock);

    nlmsg_free(msg);

    if (!associated)
        return NULL;

    return ssid;

nla_put_failure:
    nlmsg_free(msg);
    return NULL;
}

char
plugin_format()
{
    return 'w';
}

int
scan_cb(struct nl_msg *msg, void *arg)
{
    UNUSED(arg);

    int i, j;

    struct nlattr *tb[NL80211_ATTR_MAX + 1];
    struct genlmsghdr *gnlh = nlmsg_data(nlmsg_hdr(msg));
    struct nlattr *bss[NL80211_BSS_MAX + 1];
    static struct nla_policy bss_policy[NL80211_BSS_MAX + 1] = {
        [NL80211_BSS_INFORMATION_ELEMENTS] = { 0 },
        [NL80211_BSS_STATUS] = { 0 }
    };

    nla_parse(tb, NL80211_ATTR_MAX, genlmsg_attrdata(gnlh, 0), genlmsg_attrlen(gnlh, 0), NULL);

    if (!tb[NL80211_ATTR_BSS])
        return NL_SKIP;

    if (nla_parse_nested(bss, NL80211_BSS_MAX,
                 tb[NL80211_ATTR_BSS],
                 bss_policy))
        return NL_SKIP;

    if (!bss[NL80211_BSS_STATUS] || !bss[NL80211_BSS_INFORMATION_ELEMENTS])
        return NL_SKIP;

    associated = nla_get_u32(bss[NL80211_BSS_STATUS]) == NL80211_BSS_STATUS_ASSOCIATED;
    if (!associated)
        return NL_SKIP;

    uint8_t *data = nla_data(bss[NL80211_BSS_INFORMATION_ELEMENTS]);
    int len = nla_len(bss[NL80211_BSS_INFORMATION_ELEMENTS]);

    for (i = 2, j = 0; i < len && j < SSID_LEN; i++) {
        if (isprint(data[i]) && data[i] != ' ' && data[i] != '\\')
            ssid[j++] = data[i];
        else if (data[i] == ' ' && i != 0 && i != len -1)
            ssid[j++] = ' ';
        else
            break;
    }

    ssid[j] = '\0';

    return NL_SKIP;
}

int
ack_cb(struct nl_msg *msg, void *arg)
{
    UNUSED(msg);
    UNUSED(arg);

    finished = true;

    return NL_STOP;
}
