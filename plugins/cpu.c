/*
 * Copyright (c) 2012-2013, Patrick Steinhardt
 * Copyright (c) 2013, Hinnerk van Bruinehsen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdbool.h>
#include <stdio.h>
#include <syslog.h>

#include "../config.h"

#define STAT "/proc/stat"
#define MAX(x, y) ((x) > (y) ? (x) : (y))

static FILE *statFd = 0;

bool
plugin_init()
{
    statFd = fopen(STAT,"r");

    return statFd != 0;
}

void
plugin_exit()
{
    fclose(statFd);
}

const char *
plugin_status()
{
    static char buf[CPU_LEN];
    static unsigned long long lastUser, lastUserLow, lastSys, lastIdle;
    unsigned long long user, userLow, sys, idle;
    unsigned long long percent, total;

    if (statFd == 0) {
        return buf;
    }

    fflush(statFd);
    rewind(statFd);

    if (fscanf(statFd, "cpu %10Lu %10Lu %10Lu %10Lu",  &user, &userLow, &sys, &idle) != 4) {
        syslog(LOG_ERR, "error reading CPU status\n");
        statFd = freopen(STAT, "r", statFd);
    } else {
        percent = (user - lastUser) + (userLow - lastUserLow) + (sys - lastSys);
        total = MAX(percent + idle - lastIdle, 1);
        snprintf(buf, CPU_LEN, CPU_FORMAT, (percent * 100) / total);

        lastUser = user;
        lastUserLow = userLow;
        lastSys = sys;
        lastIdle = idle;
    }

    return buf;
}

char
plugin_format()
{
    return 'c';
}
