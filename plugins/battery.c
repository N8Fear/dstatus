/*
 * Copyright (c) 2012-2013, Patrick Steinhardt
 * Copyright (c) 2013-2016, Hinnerk van Bruinehsen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdbool.h>
#include <stdio.h>
#include <syslog.h>

#include "../config.h"

static FILE *batteryFd = 0;
static FILE *acFd = 0;
static int capacity;
static int ac_state;

bool
plugin_init()
{
    acFd = fopen(AC_DEV, "r");
    batteryFd = fopen(BATTERY_DEV, "r");

    if (batteryFd != 0) {
        fscanf(batteryFd, "%10i", &capacity);
        syslog(LOG_INFO, "ACPI state has been initialized.");
    }

    return batteryFd != 0;
}

void
plugin_exit()
{
    fclose(batteryFd);
    fclose(acFd);
}

const char *
plugin_status()
{
    static char buf[BATTERY_LEN];
    static unsigned int cycle = 0;
    char logo[5] = {0};

    if (cycle++ % 5 != 0 || batteryFd == 0 || acFd == 0) {
        return buf;
    }

    fflush(batteryFd);
    rewind(batteryFd);

    fflush(acFd);
    rewind(acFd);

    ac_state = 0;

    if (fscanf(acFd, "%1i", &ac_state) != 1) {
        acFd = freopen(AC_DEV, "r", acFd);
    }

    if (fscanf(batteryFd, "%10i", &capacity) != 1) {
        syslog(LOG_ERR, "error reading ACPI status, reopening\n");
        batteryFd = freopen(BATTERY_DEV, "r", batteryFd);
    } else {
        if (ac_state == 1)
            snprintf(logo, 5, "%s  ", BATTERY_CHARGING);
        else {
            if (capacity)
                snprintf(logo, 5, "%s  ", BATTERY_FULL);
            if (capacity <= 75)
                snprintf(logo, 5, "%s  ", BATTERY_HIGH);
            if (capacity <= 50)
                snprintf(logo, 5, "%s  ", BATTERY_LOW);
            if (capacity <= 25)
                snprintf(logo, 5, "%s  ", BATTERY_EMPTY);
        }
        snprintf(buf, BATTERY_LEN, "%s%i", logo, capacity);
    }

    return buf;
}

char
plugin_format()
{
    return 'b';
}
