# pkg-config
PKG_CONFIG ?= pkg-config

# paths
PREFIX = /usr
SOPATH = ${PREFIX}/lib/dstatus/

#X11INC = $(shell $(PKG_CONFIG) --cflags x11)
X11LIBS = $(shell $(PKG_CONFIG) --libs-only-l x11)

# plugins
PLUGINS = time

ifeq ($(WITH_CPU),1)
PLUGINS += cpu
endif

ifeq ($(WITH_MEMORY),1)
PLUGINS += memory
endif

ifeq ($(WITH_BATTERY),1)
PLUGINS += battery
endif

ifeq ($(WITH_VOLUME),1)
PLUGINS += volume
endif

ifeq ($(WITH_SELINUX),1)
PLUGINS += selinux
endif

ifeq ($(WITH_WIFI),1)
PLUGINS += wifi
endif

ifeq ($(WITH_MPD),1)
PLUGINS += mpd
endif

ifeq ($(WITH_IP),1)
PLUGINS += ip
endif

ifeq ($(WITH_BRIGHTNESS),1)
PLUGINS += brightness
endif

ifeq ($(WITH_LOADAVG),1)
PLUGINS += load
endif

ifeq ($(WITH_NETSTAT),1)
PLUGINS += netstat
endif

# includes and libs
LIBS  = $(X11LIBS) -ldl
FLAGS = -DSOPATH=\"${SOPATH}\"

# flags
CFLAGS += -std=gnu11 -Wall $(INCS) $(FLAGS)
LDFLAGS += -s $(LIBS)

# compiler and linker
CC ?= gcc

