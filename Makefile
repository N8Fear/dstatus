# dstatus - dynamic status
# See LICENSE file for copyright and license details.

include config.mk

SRCS = dstatus.c
OBJS = ${SRCS:.c=.o}

PLUGINSRCS = $(patsubst %,plugins/%.c,${PLUGINS})
PLUGINOBJS = ${PLUGINSRCS:.c=.so}

all: options dstatus plugins

options:
	@echo dstatus build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

dstatus: ${OBJS}
	@echo CC -o $@
	@${CC} -o $@ ${OBJS} ${LDFLAGS}

plugins: ${PLUGINOBJS}

%.o: %.c config.h config.mk
	@echo CC $<
	@${CC} -o $@ -c ${CFLAGS} $<

plugins/mpd.so: plugins/mpd.c config.h config.mk
	@echo CC -o $@
	@${CC} -o $@ -shared -fPIC ${CFLAGS} $< -lmpdclient

plugins/volume.so: plugins/volume.c config.h config.mk
	@echo CC -o $@
	@${CC} -o $@ -shared -fPIC ${CFLAGS} $< -lasound

plugins/wifi.so: plugins/wifi.c config.h config.mk
	@echo CC -o $@
	@${CC} -o $@ -shared -fPIC ${CFLAGS} -I/usr/include/libnl3 $< -lnl-genl-3 -lnl-3

%.so: %.c config.h config.mk
	@echo CC -o $@
	@${CC} -o $@ -shared -fPIC ${CFLAGS} $<

clean:
	@echo cleaning
	@rm -f dstatus *.o plugins/*.o plugins/*.so

install: all
	@echo installing executable file to ${DESTDIR}${PREFIX}/bin
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp -f dstatus ${DESTDIR}${PREFIX}/bin
	@chmod 755 ${DESTDIR}${PREFIX}/bin/dstatus
	@echo installing plugins to ${DESTDIR}${SOPATH}
	@mkdir -p ${DESTDIR}${SOPATH}
	@cp -f ${PLUGINOBJS} ${DESTDIR}${SOPATH}

uninstall:
	@echo removing executable file from ${DESTDIR}${PREFIX}/bin
	@rm -f ${DESTDIR}${PREFIX}/bin/dstatus
	@echo removing plugins from ${DESTDIR}${SOPATH}
	@rm -f ${DESTDIR}${SOPATH}/*

.PHONY: all options clean install uninstall
