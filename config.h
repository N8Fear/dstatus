/*
 * Copyright (c) 2012, Patrick Steinhardt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#define NPLUGINS 12

#define STATUS_LENGTH 512

#define BATTERY_FORMAT    "%5i"
#define BATTERY_LEN       8
#define BATTERY_FULL      "\uF213"
#define BATTERY_HIGH      "\uF214"
#define BATTERY_LOW       "\uF215"
#define BATTERY_EMPTY     "\uF212"
#define BATTERY_CHARGING  "\uF0E7"

#define VOL_FORMAT        "%s%02ld"
#define VOL_LEN           8
#define VOL_FULL          "\uF028"
#define VOL_HALF          "\uF027"
#define VOL_EMPTY         "\uF026"

#define TIME_FORMAT "[ %a %F ]-[ %H:%M ] "
#define TIME_LEN          64
#define CPU_FORMAT        "%02Ld"
#define CPU_LEN           4
#define MEM_FORMAT        "%3li"
#define MEM_LEN           4
#define MPD_FORMAT        "%a - %t"
#define MPD_LEN           64
#define BRIGHTNESS_FORMAT "%3i"
#define BRIGHTNESS_LEN    4

#define BATTERY_DEV    "/sys/class/power_supply/BAT0/capacity"
#define AC_DEV    "/sys/class/power_supply/AC/online"
#define WIRELESS_IFACE "wlan0"
#define IP_IFACE "wlan0"
#define BRIGHTNESS_DEV "nv_backlight"

#define FORMAT "[  \u21c5 %n  ]    %([  %m  ]%)[  \u262d  %c  |  \uF3B5 %M  |  Load: %l  ]%([ \uF35C  %w  ]%)%([ %v  ]%)%([  %b  ]%) %t"
